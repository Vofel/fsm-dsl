# -*- coding: utf-8 -*-
import math
import pytest
from typing import Any, Tuple
from src.state import State
from src.fsm_builder import FSMBuilder
from src.fsm import FSM, RecognitionResult


def save_sign(previous_state, current_state, inp, global_context) -> None:
    if inp == '+':
        global_context['sign'] = 1.0
    elif inp == '-':
        global_context['sign'] = -1.0
    else:
        raise ValueError(f'Unknown sign symbol {inp}')


def int_digit_loop_entry(previous_state, current_state, inp, out, global_context) -> None:
    global_context['value'] = out


def frac_digit_loop_entry(previous_state, current_state, inp, out, global_context) -> None:
    if out:
        global_context['value'] = out
    global_context['frac_power'] -= 1


def exp_loop_entry(previous_state, current_state, inp, out, global_context) -> None:
    global_context['exp_value'] *= 10
    global_context['exp_value'] += ord(inp) - ord('0')


def int_digit_output(a, b, inp, global_context):
    return (
        global_context['sign'] * (abs(global_context['value']) * 10 + ord(inp) - ord('0'))
        if inp not in '.e' else global_context['value']
    )


def frac_digit_output(a, b, inp, global_context):
    return (
        global_context['sign'] * (
            abs(global_context['value']) + pow(10, global_context['frac_power']) * (ord(inp) - ord('0'))
        )
        if inp != 'e' else global_context['value']
    )


def exp_output(a, b, inp, global_context):
    return (
        global_context['value'] * pow(
            10,
            global_context['exp_sign'] * (10 * global_context['exp_value'] + ord(inp) - ord('0'))
        )
    )


def add_transitions(current_state, inputs, next_state_name, callback):
    for inp in inputs:
        current_state.set_transition(inp, next_state_name, callback)


def scanf() -> FSM:
    # start
    start: State = State(
        'start', is_final=False,
    ).set_transition(
        'i', 'infin_state_i',
    ).set_transition(
        'n', 'nan_state_n',
    ).set_transition(
        '-', 'start', save_sign,
    ).set_transition(
        '+', 'start', save_sign,
    ).set_transition(
        '0', 'leading_sig_zeroes', int_digit_output,
    ).set_transition(
        '.', 'frac_start',
    )
    add_transitions(start, '123456789', 'int_digit_loop', int_digit_output)

    # leading_sig_zeroes
    leading_sig_zeroes: State = State(
        'leading_sig_zeroes'
    ).set_transition(
        '0', 'leading_sig_zeroes', int_digit_output,
    ).set_transition(
        '.', 'frac_digit_loop', int_digit_output,
    ).set_transition(
        'e', 'exp_start',
    )
    add_transitions(leading_sig_zeroes, '123456789', 'int_digit_loop', int_digit_output)

    # int_digit_loop
    int_digit_loop: State = State(
        'int_digit_loop'
    ).set_entry_callback(
        int_digit_loop_entry,
    ).set_transition(
        '.', 'frac_digit_loop', int_digit_output,
    ).set_transition(
        'e', 'exp_start',
    )
    add_transitions(int_digit_loop, '0123456789', 'int_digit_loop', int_digit_output)

    # frac_start
    frac_start: State = State(
        'frac_start', is_final=False
    ).set_entry_callback(
        frac_digit_loop_entry,
    )
    add_transitions(frac_start, '0123456789', 'frac_digit_loop', frac_digit_output)

    # frac_digit_loop
    frac_digit_loop: State = State(
        'frac_digit_loop'
    ).set_entry_callback(
        frac_digit_loop_entry,
    ).set_transition(
        'e', 'exp_start',
    )
    add_transitions(frac_digit_loop, '0123456789', 'frac_digit_loop', frac_digit_output)

    # exp_start
    exp_start: State = State(
        'exp_start', is_final=False,
    ).set_transition(
        '+', 'leading_exp_digit',
    ).set_transition(
        '-', 'leading_exp_digit', lambda a, b, c, global_context: global_context.update({'exp_sign': -1}),
    ).set_transition(
        '0', 'leading_exp_zeroes',
    )
    add_transitions(exp_start, '123456789', 'leading_exp_loop', exp_output)

    # leading_exp_digit
    leading_exp_digit: State = State(
        'leading_exp_digit', is_final=False,
    ).set_transition(
        '0', 'leading_exp_zeroes',
    )
    add_transitions(leading_exp_digit, '123456789', 'leading_exp_loop', exp_output)

    # leading_exp_zeroes
    leading_exp_zeroes: State = State(
        'leading_exp_zeroes'
    ).set_transition(
        '0', 'leading_exp_zeroes', exp_output,
    )
    add_transitions(leading_exp_zeroes, '123456789', 'leading_exp_loop', exp_output)

    # leading_exp_loop
    leading_exp_loop: State = State(
        'leading_exp_loop'
    ).set_entry_callback(
        exp_loop_entry,
    )
    add_transitions(leading_exp_loop, '0123456789', 'leading_exp_loop', exp_output)


    fsm: FSM = FSMBuilder(
        initial_global_context={
            'sign': 1.0,
            'value': 0.0,
            'frac_power': 0,
            'exp_sign': 1,
            'exp_value': 0,
        },
    ).add_state(
        start
    ).add_state(
        State('infin_state_i', is_final=False).set_transition('n', 'infin_state_in'),
    ).add_state(
        State('infin_state_in', is_final=False).set_transition('f', 'infin_state_inf', lambda a, b, c, global_context: global_context['sign'] * float('inf')),
    ).add_state(
        State('infin_state_inf'),
    ).add_state(
        State('nan_state_n', is_final=False).set_transition('a', 'nan_state_na'),
    ).add_state(
        State('nan_state_na', is_final=False).set_transition('n', 'nan_state_nan', lambda *_: float('nan')),
    ).add_state(
        State('nan_state_nan'),
    ).add_state(
        leading_sig_zeroes,
    ).add_state(
        int_digit_loop,
    ).add_state(
        frac_start,
    ).add_state(
        frac_digit_loop,
    ).add_state(
        exp_start,
    ).add_state(
        leading_exp_digit,
    ).add_state(
        leading_exp_zeroes,
    ).add_state(
        leading_exp_loop,
    ).set_initial_state(
        'start',
    ).build()

    return fsm


def test_scanf_nan() -> None:
    input_str = 'NaN'
    fsm = scanf()
    for char in input_str.lower():
        res = fsm.step(char)
        if res[0] == RecognitionResult.INAPPROPRIATE_SYMBOL or res[0] == RecognitionResult.ERROR_OCCURRED:
            break
    assert res[0] == RecognitionResult.RECOGNIZED
    assert math.isnan(res[1])


@pytest.mark.parametrize(
    'input_str, expected',
    [
        ('inf', (RecognitionResult.RECOGNIZED, float('inf'))),
        ('+inf', (RecognitionResult.RECOGNIZED, float('inf'))),
        ('-inf', (RecognitionResult.RECOGNIZED, float('-inf'))),
        ('-+-inf', (RecognitionResult.RECOGNIZED, float('-inf'))),
        ('0', (RecognitionResult.RECOGNIZED, 0.0)),
        ('+0', (RecognitionResult.RECOGNIZED, 0.0)),
        ('-00000', (RecognitionResult.RECOGNIZED, -0.0)),
        ('0.', (RecognitionResult.RECOGNIZED, 0.0)),
        ('.', (RecognitionResult.NON_FINAL_STATE, None)),
        ('-321', (RecognitionResult.RECOGNIZED, -321)),
        ('00123', (RecognitionResult.RECOGNIZED, 123.0)),
        ('-666е666', (RecognitionResult.INAPPROPRIATE_SYMBOL, None)),
        ('1337.', (RecognitionResult.RECOGNIZED, 1337.0)),
        ('1337.7331', (RecognitionResult.RECOGNIZED, 1337.7331)),
        ('-.25', (RecognitionResult.RECOGNIZED, -0.25)),
        ('.0322', (RecognitionResult.RECOGNIZED, 0.0322)),
        ('5.e000', (RecognitionResult.RECOGNIZED, 5.0)),
        ('5.7e000', (RecognitionResult.RECOGNIZED, 5.7)),
        ('5.7654e2', (RecognitionResult.RECOGNIZED, 576.54)),
        ('5.7654e+2', (RecognitionResult.RECOGNIZED, 576.54)),
        ('5.7654e-2', (RecognitionResult.RECOGNIZED, 0.057654)),
        ('5.74e++2', (RecognitionResult.INAPPROPRIATE_SYMBOL, None)),
        ('0e111', (RecognitionResult.RECOGNIZED, 0.0)),
        ('2e10', (RecognitionResult.RECOGNIZED, 2e10)),
        ('2e', (RecognitionResult.NON_FINAL_STATE, None)),
        ('2.2e', (RecognitionResult.NON_FINAL_STATE, None)),
    ]
)
def test_scanf(input_str: str, expected: Tuple[RecognitionResult, Any]) -> None:
    fsm = scanf()
    res = fsm.recognize(list(input_str), input_as_tuples=False)
    assert res[0] == expected[0]
    if expected[0] == RecognitionResult.RECOGNIZED:
        assert res[1][-1] == expected[1]
        assert isinstance(res[1][-1], float)
