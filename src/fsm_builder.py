# -*- coding: utf-8 -*-
from __future__ import annotations
from typing import Dict, Optional, Hashable, List, Tuple, Any
from copy import deepcopy
import numpy as np
from src.state import State
from src.fsm import FSM


class FSMBuilderError(Exception):
    pass


class FSMBuilderWarning(Exception):
    pass


class FSMBuilder:
    def __init__(self, initial_global_context: Optional[Dict[Hashable, Any]] = None) -> None:
        self.__initial_state_name: Optional[Hashable] = None
        self.__states: Dict[Hashable, State] = {}
        self.global_context: Optional[Dict[Hashable, Any]] = initial_global_context

    @property
    def initial_state_name(self) -> Optional[Hashable]:
        return self.__initial_state_name

    def get_state_names(self) -> List[Hashable]:
        # since python 3.7 dict is always ordered so the method returns state names in fixed order.
        return list(self.__states)

    def get_state(self, name: Hashable) -> Optional[State]:
        return self.__states.get(name)

    def generate_reachability_matrix(self) -> Tuple[np.ndarray, Dict[Hashable, int]]:
        """
        Returns reachability_matrix (as np.ndarray) and
        state names mapping on indices in corresponding lines and columns in matrix.
        """
        states_count = len(self.__states)
        mapping = {}
        for state_name, state_idx in zip(self.__states, range(states_count)):
            mapping[state_name] = state_idx
        one_step_rmatrix: np.ndarray = np.zeros((states_count, states_count))
        for state_name in self.__states:
            state: State = self.__states[state_name]
            one_step_reachables: List[Hashable] = state.get_one_step_reachable()
            for osr_name in one_step_reachables:
                if osr_name not in self.__states:
                    continue
                one_step_rmatrix[mapping[state_name], mapping[osr_name]] = 1
        cur_step_rmatrix: np.ndarray = np.eye(states_count)
        res_rmatrix: np.ndarray = np.eye(states_count)
        for _ in range(states_count-1):
            cur_step_rmatrix = cur_step_rmatrix.dot(one_step_rmatrix)
            res_rmatrix += cur_step_rmatrix
        return (res_rmatrix, mapping)

    def set_initial_state(self, name: Hashable) -> FSMBuilder:
        self.__initial_state_name = deepcopy(name)
        return self

    def add_state(self, state: State) -> FSMBuilder:
        if state.name in self.__states:
            raise FSMBuilderError(f'A state with name "{state.name}" has already been added')
        self.__states[state.name] = deepcopy(state)
        return self

    def validate_initial_state_name(self) -> FSMBuilder:
        if self.__initial_state_name in self.__states:
            return self
        raise FSMBuilderError(f'A state with name "{self.__initial_state_name}" (supposed to be initial) has not been added yet')

    def validate_fsm_consistency(self) -> FSMBuilder:
        """
        - Checks if all reachable states exist in FSM.
        - Checks if all other states are reachable from initial state.
        - Checks if at least one final state is reachable from any non-final state.
        Note: validate_fsm_consistency() should be used only after validate_initial_state_name()
        """
        grm_tuple: Tuple[np.ndarray, Dict[Hashable, int]] = self.generate_reachability_matrix()
        rmatrix: np.ndarray = grm_tuple[0]
        mapping: Dict[Hashable, int] = grm_tuple[1]
        for state_name in self.__states:
            state: State = self.__states[state_name]
            osrs: List[Hashable] = state.get_one_step_reachable()
            for osr in osrs:
                if osr not in self.__states:
                    raise FSMBuilderError(f'Unknown within FSM state with name "{osr}" is reachable from state with name "{state_name}"')
            if not rmatrix[mapping[self.__initial_state_name], mapping[state_name]]:
                raise FSMBuilderError(f'State "{state_name}" is not reachable from initial state "{self.__initial_state_name}"')
            if not state.is_final:
                if not any(map(
                    lambda stn: self.__states[stn].is_final and rmatrix[mapping[state_name], mapping[stn]],
                    self.__states
                )):
                    raise FSMBuilderWarning(f'None of final states is reachable from state "{state_name}"')
        return self

    def validate(self) -> FSMBuilder:
        return self.validate_initial_state_name().validate_fsm_consistency()

    def build(self, ignore_warnings: bool = False) -> FSM:
        try:
            self.validate()
        except FSMBuilderWarning:
            if not ignore_warnings:
                raise
        fsm: FSM = FSM.__new__(FSM)
        fsm_class_prefix = "_" + FSM.__name__
        fsm.__dict__[fsm_class_prefix+"__initial_state_name"] = deepcopy(self.__initial_state_name)
        fsm.__dict__[fsm_class_prefix+"__states"] = deepcopy(self.__states)
        fsm.__dict__[fsm_class_prefix+"__current_state_name"] = fsm.__dict__[fsm_class_prefix+"__initial_state_name"]
        fsm.__dict__["global_context"] = deepcopy(self.global_context)
        return fsm
