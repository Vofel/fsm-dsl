# -*- coding: utf-8 -*-
from typing import Optional, Hashable, List, Any, Tuple, Dict
from src.state import State
from src.fsm_builder import FSMBuilder
from src.fsm import FSM, RecognitionResult, ErrorLocation


def test_fsm() -> None:
    log: List[str] = []

    def log_exit_cb(cur_st: State, new_st: State, input: Hashable, gc: Optional[Dict[Hashable, Any]]):
        log.append(f"From {cur_st.name} to {new_st.name} with {input}")

    def err_cb(prev_st: State, new_st: State, input: Hashable, output: Any, gc: Optional[Dict[Hashable, Any]]) -> None:
        raise Exception("Entered error state")

    fsm: FSM = FSMBuilder().add_state(
        State("st1").set_transition(1, "st2", None).set_transition(2, "st3", lambda a, b, c, d: 3).set_exit_callback(log_exit_cb)
    ).add_state(
        State("st2").set_transition(1, "st3", lambda a, b, c, d: 3).set_transition(-1, "st1", lambda a, b, c, d: 1).set_exit_callback(log_exit_cb)
    ).add_state(
        State("st3", is_final=False).set_transition(-1, "st2", lambda a, b, c, d: 2).set_transition(-2, "st1", lambda a, b, c, d: 1).set_exit_callback(log_exit_cb).set_transition(322, "st_er", lambda a, b, c, d: "ggwp")
    ).add_state(
        State("st_er", is_final=False).set_entry_callback(err_cb)
    ).set_initial_state(
        "st1"
    ).build(ignore_warnings=True)
    assert(fsm.initial_state_name == "st1" and fsm.current_state_name == "st1")
    res: Tuple[RecognitionResult, Any, Optional[Tuple[ErrorLocation, Exception]]] = fsm.step(1)
    assert (res[0] == RecognitionResult.RECOGNIZED and res[1] is None and res[2] is None)
