# -*- coding: utf-8 -*-
from __future__ import annotations
from typing import Dict, Optional, Tuple, Callable, Any, Hashable, List


class State:
    def __init__(self, name: Hashable, is_final: bool = True, initial_context: Optional[Dict[Hashable, Any]] = None) -> None:
        self.__name: Hashable = name
        self.__is_final: bool = is_final
        # entry_callback: previous_state, current_state, input, output, global_context -> None
        self.__entry_callback: Optional[Callable[[State, State, Hashable, Any, Optional[Dict[Hashable, Any]]], None]] = None
        # exit_callback: current_state, new_state, input, global_context -> None
        self.__exit_callback: Optional[Callable[[State, State, Hashable, Optional[Dict[Hashable, Any]]], None]] = None
        # input: (new_state_name, (output_function: old_state, new_state, input, global_context -> output))
        self.__transition_table: Dict[Hashable, Tuple[Hashable, Optional[Callable[[State, State, Hashable, Optional[Dict[Hashable, Any]]], Any]]]] = {}
        self.context: Optional[Dict[Hashable, Any]] = initial_context

    @property
    def name(self) -> Hashable:
        return self.__name

    @property
    def is_final(self) -> bool:
        return self.__is_final

    @property
    def entry_callback(self) -> Optional[Callable[[State, State, Hashable, Any, Optional[Dict[Hashable, Any]]], None]]:
        return self.__entry_callback

    @property
    def exit_callback(self) -> Optional[Callable[[State, State, Hashable, Optional[Dict[Hashable, Any]]], None]]:
        return self.__exit_callback

    def get_transition(self, input: Hashable) -> Optional[Tuple[Hashable, Optional[Callable[[State, State, Hashable, Optional[Dict[Hashable, Any]]], Any]]]]:
        return self.__transition_table.get(input)

    def get_one_step_reachable(self) -> List[Hashable]:
        return list(map(lambda t: t[0], self.__transition_table.values()))

    def set_entry_callback(self, callback: Optional[Callable[[State, State, Hashable, Any, Optional[Dict[Hashable, Any]]], None]] = None) -> State:
        self.__entry_callback = callback
        return self

    def set_exit_callback(self, callback: Optional[Callable[[State, State, Hashable, Optional[Dict[Hashable, Any]]], None]] = None) -> State:
        self.__exit_callback = callback
        return self

    def set_transition(self, input: Hashable, new_state_name: Hashable, output_func: Optional[Callable[[State, State, Hashable, Optional[Dict[Hashable, Any]]], Any]] = None) -> State:
        self.__transition_table[input] = (new_state_name, output_func)
        return self
