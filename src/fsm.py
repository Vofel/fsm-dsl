# -*- coding: utf-8 -*-
from copy import deepcopy
from typing import cast, Dict, Optional, Hashable, List, Any, Tuple, Callable, Union, Sequence
from enum import Enum
from src.state import State
from src.context_manager import ContextManager


class FSMError(Exception):
    pass


class RecognitionResult(Enum):
    RECOGNIZED = 1
    NON_FINAL_STATE = 2
    INAPPROPRIATE_SYMBOL = 3
    ERROR_OCCURRED = 4


class FunctionUsage(Enum):
    STORE_CONTEXT = 1
    DONT_STORE_CONTEXT = 2
    IGNORE_FUNCTION = 3


class ReturnTypeCCF(Enum):
    PASS = 1
    OUTPUT_VALUE = 2
    ERROR = 3


class ErrorLocation(Enum):
    EXIT_CB = 1
    OUTPUT_FUNC = 2
    ENTRY_CB = 3


class FSM:
    def __init__(self) -> None:
        self.__initial_state_name: Hashable
        self.__states: Dict[Hashable, State]
        self.__current_state_name: Hashable
        self.global_context: Optional[Dict[Hashable, Any]]
        raise SyntaxWarning("Objects of class FSM should only be created by FSMBuilder")

    @property
    def initial_state_name(self) -> Hashable:
        return self.__initial_state_name

    @property
    def current_state_name(self) -> Hashable:
        return self.__current_state_name

    def get_state_names(self) -> List[Hashable]:
        # since python 3.7 dict is always ordered so the method returns state names in fixed order.
        return list(self.__states)

    def get_state(self, name: Hashable) -> Optional[State]:
        return self.__states.get(name)

    def reset_state(self) -> None:
        self.__current_state_name = self.__initial_state_name

    @staticmethod
    def __call_custom_function(
        func: Optional[Callable],
        usage: FunctionUsage,
        *func_args: Any,
        need_output_value: bool = False
    ) -> Tuple[ReturnTypeCCF, Optional[ContextManager], Any]:
        "res[2] can be: None (if PASS) or OUTPUT (if OUTPUT_VALUE) or ExceptionValue (if ERROR)"
        if ((func is None) or (usage == FunctionUsage.IGNORE_FUNCTION)) and (not need_output_value):
            return (ReturnTypeCCF.PASS, None, None)
        elif ((func is None) or (usage == FunctionUsage.IGNORE_FUNCTION)) and need_output_value:
            return (ReturnTypeCCF.OUTPUT_VALUE, None, None)
        else:
            some_func: Callable = cast(Callable, func)
            func_context_manager: Optional[ContextManager] = None
            if usage == FunctionUsage.STORE_CONTEXT:
                some_func_context_manager: ContextManager = ContextManager(some_func)
                some_func_context_manager.save()
                func_context_manager = some_func_context_manager
            elif usage == FunctionUsage.DONT_STORE_CONTEXT:
                pass
            else:
                raise FSMError(f"Unexpected FunctionUsage value: {usage}")

            res: Any = None
            try:
                res = some_func(*func_args)
            except Exception as cb_exception:
                return (ReturnTypeCCF.ERROR, func_context_manager, cb_exception)
            else:
                if need_output_value:
                    return (ReturnTypeCCF.OUTPUT_VALUE, func_context_manager, res)
                else:
                    return (ReturnTypeCCF.PASS, func_context_manager, None)

    @staticmethod
    def __restore_contexts(contexts: List[Optional[ContextManager]]) -> None:
        "Restores from last to first"
        for context in reversed(contexts):
            if context is not None:
                (cast(ContextManager, context)).restore()

    def step(
        self,
        input: Hashable,
        exit_callback_usage: FunctionUsage = FunctionUsage.STORE_CONTEXT,
        entry_callback_usage: FunctionUsage = FunctionUsage.STORE_CONTEXT,
        transition_func_usage: FunctionUsage = FunctionUsage.STORE_CONTEXT,
        store_states_context: bool = False,
        store_global_context: bool = False
    ) -> Tuple[RecognitionResult, Any, Optional[Tuple[ErrorLocation, Exception]]]:
        """
        Order:
        1. Exit callback.
        2. Output function (if IGNORE_FUNCTION, output is None).
        3. Entry callback.
        """
        # transition
        current_state: State = self.__states[self.__current_state_name]
        transition_res: Optional[Tuple[Hashable, Optional[Callable[[State, State, Hashable, Optional[Dict[Hashable, Any]]], Any]]]] = current_state.get_transition(input)
        if transition_res is None:
            return (RecognitionResult.INAPPROPRIATE_SYMBOL, None, None)
        new_state_name: Hashable = transition_res[0]
        new_state: State = self.__states[new_state_name]
        output_function: Optional[Callable[[State, State, Hashable, Optional[Dict[Hashable, Any]]], Any]] = transition_res[1]
        output: Any = None  # (have not used transition function yet)

        # has an element corresponding to every call of custom function (if we don't need to restore context, element is None)
        saved_contexts: List[Optional[ContextManager]] = []
        cur_state_context_stored: Optional[Dict[Hashable, Any]]
        new_state_context_stored: Optional[Dict[Hashable, Any]]
        global_context_stored: Optional[Dict[Hashable, Any]]
        if store_states_context:
            cur_state_context_stored = deepcopy(current_state.context)
            new_state_context_stored = deepcopy(new_state.context)
        if store_global_context:
            global_context_stored = deepcopy(self.global_context)

        # exit callback (old state)
        call_res: Tuple[ReturnTypeCCF, Optional[ContextManager], Any] = self.__call_custom_function(
            current_state.exit_callback,
            exit_callback_usage,
            current_state,
            new_state,
            input,
            self.global_context,
            need_output_value=False
        )
        call_return_type: ReturnTypeCCF = call_res[0]
        call_stored_context: Optional[ContextManager] = call_res[1]
        call_return_res: Any = call_res[2]
        saved_contexts.append(call_stored_context)
        if call_return_type == ReturnTypeCCF.PASS:
            pass
        elif call_return_type == ReturnTypeCCF.ERROR:
            self.__restore_contexts(saved_contexts)
            if store_states_context:
                current_state.context = cur_state_context_stored
                new_state.context = new_state_context_stored
            if store_global_context:
                self.global_context = global_context_stored
            return (RecognitionResult.ERROR_OCCURRED, output, (ErrorLocation.EXIT_CB, call_return_res))
        elif call_return_type == ReturnTypeCCF.OUTPUT_VALUE:
            raise FSMError("After call exit callback ReturnTypeCCF is OUTPUT_VALUE")
        else:
            raise FSMError(f"Unexpected ReturnTypeCCF value: {call_return_type}")

        # output func
        call_res = self.__call_custom_function(
            output_function,
            transition_func_usage,
            current_state,
            new_state,
            input,
            self.global_context,
            need_output_value=True
        )
        call_return_type = call_res[0]
        call_stored_context = call_res[1]
        call_return_res = call_res[2]
        saved_contexts.append(call_stored_context)
        if call_return_type == ReturnTypeCCF.PASS:
            raise FSMError("After call output function ReturnTypeCCF is PASS")
        elif call_return_type == ReturnTypeCCF.ERROR:
            self.__restore_contexts(saved_contexts)
            if store_states_context:
                current_state.context = cur_state_context_stored
                new_state.context = new_state_context_stored
            if store_global_context:
                self.global_context = global_context_stored
            return (RecognitionResult.ERROR_OCCURRED, output, (ErrorLocation.OUTPUT_FUNC, call_return_res))
        elif call_return_type == ReturnTypeCCF.OUTPUT_VALUE:
            output = call_return_res
        else:
            raise FSMError(f"Unexpected ReturnTypeCCF value: {call_return_type}")

        # entry callback (new state)
        call_res = self.__call_custom_function(
            new_state.entry_callback,
            entry_callback_usage,
            current_state,
            new_state,
            input,
            output,
            self.global_context,
            need_output_value=False
        )
        call_return_type = call_res[0]
        call_stored_context = call_res[1]
        call_return_res = call_res[2]
        saved_contexts.append(call_stored_context)
        if call_return_type == ReturnTypeCCF.PASS:
            pass
        elif call_return_type == ReturnTypeCCF.ERROR:
            self.__restore_contexts(saved_contexts)
            if store_states_context:
                current_state.context = cur_state_context_stored
                new_state.context = new_state_context_stored
            if store_global_context:
                self.global_context = global_context_stored
            return (RecognitionResult.ERROR_OCCURRED, output, (ErrorLocation.ENTRY_CB, call_return_res))
        elif call_return_type == ReturnTypeCCF.OUTPUT_VALUE:
            raise FSMError("After call entry callback ReturnTypeCCF is OUTPUT_VALUE")
        else:
            raise FSMError(f"Unexpected ReturnTypeCCF value: {call_return_type}")

        self.__current_state_name = new_state_name
        rec_res: RecognitionResult = RecognitionResult.RECOGNIZED if new_state.is_final else RecognitionResult.NON_FINAL_STATE
        return (rec_res, output, None)

    def recognize(
        self,
        input_vec: Union[Sequence[Hashable], Sequence[Tuple[Hashable, Tuple[FunctionUsage, FunctionUsage, FunctionUsage, bool, bool]]]],
        exit_callback_usage: FunctionUsage = FunctionUsage.STORE_CONTEXT,
        entry_callback_usage: FunctionUsage = FunctionUsage.STORE_CONTEXT,
        transition_func_usage: FunctionUsage = FunctionUsage.STORE_CONTEXT,
        store_states_context: bool = False,
        store_global_context: bool = False,
        input_as_tuples: bool = True
    ) -> Tuple[RecognitionResult, List[Any], Optional[Tuple[ErrorLocation, Exception]]]:
        """
        Usage tuple: (<exit_callback_usage>, <entry_callback_usage>, <transition_func_usage>, <store_states_context>, <store_global_context>).
        If input_as_tuples: every input in input_vec is being processed with corresponding usage tuple.
        If not input_as_tuples: every input in list is being processed with 'exit_callback_usage', 'entry_callback_usage', 'transition_func_usage', 'store_states_context', 'store_global_context'.
        Even if we got exception or inapropreate symbol, output_vec of recognized part will be returned.
        """
        input_vector: Sequence[Tuple[Hashable, Tuple[FunctionUsage, FunctionUsage, FunctionUsage, bool, bool]]]
        if input_as_tuples:
            input_vector = cast(Sequence[Tuple[Hashable, Tuple[FunctionUsage, FunctionUsage, FunctionUsage, bool, bool]]], input_vec)
        else:
            input_vector = list(map(
                lambda inp: (inp, (exit_callback_usage, entry_callback_usage, transition_func_usage, store_states_context, store_global_context)),
                input_vec
            ))

        cur_rec_res: RecognitionResult = RecognitionResult.RECOGNIZED if (self.__states[self.__current_state_name]).is_final else RecognitionResult.NON_FINAL_STATE
        output_vec: List[Any] = []
        for inp in input_vector:
            step_res: Tuple[RecognitionResult, Any, Optional[Tuple[ErrorLocation, Exception]]] = self.step(
                inp[0],
                inp[1][0],
                inp[1][1],
                inp[1][2],
                inp[1][3],
                inp[1][4]
            )
            if ((step_res[0] == RecognitionResult.RECOGNIZED) or (step_res[0] == RecognitionResult.NON_FINAL_STATE)):
                cur_rec_res = step_res[0]
                output_vec.append(step_res[1])
            elif (step_res[0] == RecognitionResult.INAPPROPRIATE_SYMBOL):
                return (RecognitionResult.INAPPROPRIATE_SYMBOL, output_vec, None)
            elif (step_res[0] == RecognitionResult.ERROR_OCCURRED):
                return (RecognitionResult.ERROR_OCCURRED, output_vec, step_res[2])
            else:
                raise FSMError(f"Unexpected RecognitionResult value: {step_res[0]}")
        return (cur_rec_res, output_vec, None)
