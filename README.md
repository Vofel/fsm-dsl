# fsm-dsl

Установка пакетов:

```
pip install -r requirements.txt
pip install -r tests/requirements.txt
```

Запуск линтера flake8 (из корня проекта):

```
flake8 .
```

Запуск статического типизатора mypy (из корня проекта):

```
mypy .
```

Запуск тестов pytest (из корня проекта):

```
pytest
```
