# -*- coding: utf-8 -*-
from typing import Optional, Hashable, List, Tuple, Any, Dict, cast
import string
from src.state import State
from src.fsm_builder import FSMBuilder
from src.fsm import FSM, RecognitionResult, ErrorLocation


def add_whitespace_transitions(st: State) -> State:
    for sym in string.whitespace:
        st.set_transition(sym, st.name)
    return st


def build_ctrlf_recognizer(pattern: str) -> FSM:
    """
    Builds case and space insensitive string recognizer.
    Symbols are numerated ignoring whitespace.
    """

    def update_counter(old_st: State, new_st: State, input: Hashable, gl_context: Optional[Dict[Hashable, Any]]):
        some_gl_context: Dict[Hashable, Any] = cast(Dict[Hashable, Any], gl_context)
        some_gl_context["counter"] += 1

    pattern = pattern.translate({ord(c): None for c in string.whitespace})
    assert pattern

    initial_state_name: Hashable = "init"
    final_state_name: Hashable = (pattern[len(pattern)-1], len(pattern)-1)
    initial_state: State = State(initial_state_name, is_final=False)
    final_state: State = State(final_state_name, is_final=True)
    builder: FSMBuilder = FSMBuilder({"counter": 0})
    next_state_name: Hashable = final_state_name
    # from last to first
    for i in range(len(pattern)-2, -1, -1):
        cur_state: State = State((pattern[i], i), is_final=False).set_transition(
            pattern[i+1].lower(), next_state_name, lambda o, n, input, d: input
        ).set_transition(
            pattern[i+1].upper(), next_state_name, lambda o, n, input, d: input
        ).set_exit_callback(update_counter)
        add_whitespace_transitions(cur_state)
        builder.add_state(cur_state)
        next_state_name = cur_state.name

    initial_state.set_transition(
        pattern[0].lower(), (pattern[0], 0), lambda o, n, input, d: input
    ).set_transition(
        pattern[0].upper(), (pattern[0], 0), lambda o, n, input, d: input
    ).set_exit_callback(update_counter)
    add_whitespace_transitions(initial_state)
    add_whitespace_transitions(final_state)
    builder.add_state(
        final_state
    ).add_state(
        initial_state
    ).set_initial_state(initial_state_name)
    return builder.build()


def find_disjoint(pattern: str, text: str) -> List[Tuple[int, int]]:
    recognizer: FSM = build_ctrlf_recognizer(pattern)
    some_global_context: Dict[Hashable, Any] = cast(Dict[Hashable, Any], recognizer.global_context)
    res: List[Tuple[int, int]] = []
    i: int = 0
    begin: int = 0
    in_process: bool = False
    while (i < len(text)):
        if (not in_process) and (text[i] in string.whitespace):
            i += 1
            begin = i
            continue
        rec_res: Tuple[RecognitionResult, Any, Optional[Tuple[ErrorLocation, Exception]]] = recognizer.step(text[i])
        if rec_res[0] == RecognitionResult.ERROR_OCCURRED:
            raise Exception(rec_res[2])
        elif rec_res[0] == RecognitionResult.INAPPROPRIATE_SYMBOL:
            in_process = False
            recognizer.reset_state()
            some_global_context["counter"] = 0
            begin = i + 1
        elif rec_res[0] == RecognitionResult.NON_FINAL_STATE:
            in_process = True
        elif rec_res[0] == RecognitionResult.RECOGNIZED:
            res.append((begin, begin + some_global_context["counter"]))
            in_process = False
            begin = i + 1
            recognizer.reset_state()
            some_global_context["counter"] = 0
        else:
            raise Exception("Unexpected RecognitionResult")
        i += 1
    return res


def test_ctrlf_recognition() -> None:
    recognizer: FSM = build_ctrlf_recognizer("      Eqw Ee ")
    text: str = " eq Wee      "
    res: Tuple[RecognitionResult, List[Any], Optional[Tuple[ErrorLocation, Exception]]] = recognizer.recognize(list(text), input_as_tuples=False)
    assert res == (RecognitionResult.RECOGNIZED, [None, 'e', 'q', None, 'W', 'e', 'e', None, None, None, None, None, None], None)


def test_ctrlf_search() -> None:
    search_res: List[Tuple[int, int]] = find_disjoint("to", "TOTO qwe qwe t   oooto qwdtsodw23TTTodqTqweo")
    assert search_res == [(0, 2), (2, 4), (13, 18), (20, 22), (35, 37)]
