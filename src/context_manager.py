# -*- coding: utf-8 -*-
import copy
import inspect
import sys
from dis import Bytecode
from typing import Any, Callable, Optional


class ContextManager:
    def __init__(self, func: Optional[Callable] = None):
        self.func: Optional[Callable] = func
        self.globals: list[Any] = []
        self.free_vars: dict[str, Any] = {}

    def save(self):
        cvs = inspect.getclosurevars(self.func)

        # free vars (enclosed)
        self.free_vars = copy.deepcopy(cvs.nonlocals)

        # globals from the same module
        gvars = cvs.globals
        # get caller
        frame = inspect.stack()[1]
        caller_module = inspect.getmodule(frame[0])

        self.globals = [
            (caller_module, gvar, copy.deepcopy(val))
            for gvar, val in gvars.items()
            if not inspect.ismodule(val)
        ]

        # globals from imports
        modules = {gvar for gvar, val in gvars.items() if inspect.ismodule(val)}
        module_components = []
        module = name = value = None
        for inst in Bytecode(self.func):
            # print("%d: %s(%s)" % (inst.offset, inst.opname, inst.argrepr))
            if inst.opname == 'LOAD_GLOBAL' and inst.argrepr in modules and not module_components:
                module_components.append(inst.argrepr)
            elif inst.opname == 'LOAD_ATTR' and module_components:
                module_components.append(inst.argrepr)
            elif inst.opname == 'STORE_ATTR' and module_components:
                module_components.append(inst.argrepr)
            elif inst.opname == 'DUP_TOP':
                continue
            else:
                if not module_components:
                    continue
                name = module_components.pop()
                module = '.'.join(module_components)
                module = sys.modules[module]
                value = copy.deepcopy(getattr(module, name))
                self.globals.append((module, name, value))
                module_components.clear()
                if inst.opname == 'LOAD_GLOBAL' and inst.argrepr in modules:
                    module_components.append(inst.argrepr)

    def restore(self):
        # free vars (enclosed)
        closure = self.func.__closure__
        if self.free_vars:
            for cell, key in zip(closure, self.free_vars):
                cell.cell_contents = self.free_vars[key]
        # globals
        for gvar in self.globals:
            setattr(gvar[0], gvar[1], gvar[2])
