# -*- coding: utf-8 -*-
import inspect
import json

import fixture.module
import fixture.nested_module.module
from src.context_manager import ContextManager


GLOBAL_IMMUTABLE = 10
GLOBAL_MUTABLE = [1, 2, 3, 4, 5]


def globals_callback():
    def callback():
        global GLOBAL_IMMUTABLE
        global GLOBAL_MUTABLE
        GLOBAL_IMMUTABLE += 5
        GLOBAL_MUTABLE.clear()

        1 / 0
    return callback


def test_globals():
    clb = globals_callback()
    manager = ContextManager(clb)
    manager.save()
    try:
        clb()
    except ZeroDivisionError:
        assert GLOBAL_IMMUTABLE == 15
        assert GLOBAL_MUTABLE == []
        manager.restore()
        assert GLOBAL_IMMUTABLE == 10
        assert GLOBAL_MUTABLE == [1, 2, 3, 4, 5]


def built_in_globals_callback():
    def callback():
        json.__author__ = 'Yoink, mine now!'

        1 / 0
    return callback


def test_built_in_globals():
    clb = built_in_globals_callback()
    manager = ContextManager(clb)
    manager.save()
    try:
        clb()
    except ZeroDivisionError:
        assert json.__author__ == 'Yoink, mine now!'
        manager.restore()
        assert json.__author__ == 'Bob Ippolito <bob@redivi.com>'


def imported_globals_callback():
    def callback():
        fixture.module.MODULE_IMMUTABLE = 'it_was_an_int'
        fixture.nested_module.module.MODULE_IMMUTABLE = 10  # was a string
        fixture.module.MODULE_MUTABLE.pop()
        fixture.nested_module.module.MODULE_MUTABLE.append('str')

        1 / 0
    return callback


def test_imported_globals():
    clb = imported_globals_callback()
    manager = ContextManager(clb)
    manager.save()
    try:
        clb()
    except ZeroDivisionError:
        assert fixture.module.MODULE_IMMUTABLE == 'it_was_an_int'
        assert fixture.nested_module.module.MODULE_IMMUTABLE == 10
        assert fixture.module.MODULE_MUTABLE == [1, 2]
        assert fixture.nested_module.module.MODULE_MUTABLE == [3, 2, 1, 'str']
        manager.restore()
        assert fixture.module.MODULE_IMMUTABLE == 1000
        assert fixture.nested_module.module.MODULE_IMMUTABLE == 'immutable'
        assert fixture.module.MODULE_MUTABLE == [1, 2, 3]
        assert fixture.nested_module.module.MODULE_MUTABLE == [3, 2, 1]


def free_variables_callback():
    free_immutable = 15
    free_mutable = [3, 2, 2]

    def callback():
        nonlocal free_immutable
        nonlocal free_mutable
        free_immutable -= 10
        free_mutable = [1, 3, 3, 7]

        1 / 0
    return callback


def test_free_variables():
    clb = free_variables_callback()
    manager = ContextManager(clb)
    manager.save()
    try:
        clb()
    except ZeroDivisionError:
        cvs = inspect.getclosurevars(clb)
        assert cvs.nonlocals['free_immutable'] == 5
        assert cvs.nonlocals['free_mutable'] == [1, 3, 3, 7]
        manager.restore()
        cvs = inspect.getclosurevars(clb)
        assert cvs.nonlocals['free_immutable'] == 15
        assert cvs.nonlocals['free_mutable'] == [3, 2, 2]
